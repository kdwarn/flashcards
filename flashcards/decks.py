"""Load and save decks; add cards to decks."""
from contextlib import contextmanager
from dataclasses import dataclass
from pathlib import Path
import sqlite3
from typing import Optional, List
import string

import click

DB_DIR_NAME = ".flashcards"
DB_FILE = "sqlite.db"
DB_PATH = Path.home() / DB_DIR_NAME / DB_FILE


@dataclass
class Deck:
    """A Deck is a container of flashcards."""

    id: int
    name: str
    description: str
    current_deck: int
    cards: list

    def __str__(self):
        return self.name


def db_dir() -> Path:
    """Get the absolute path of the directory where the db will be located."""
    return Path.home() / DB_DIR_NAME


def create_db_dir(path=db_dir()) -> None:
    """Create db_dir if it doesn't exist."""
    if not path.exists():
        path.mkdir()


@contextmanager
def db(path):
    """
    Create a context manager for all database work.

    If *path* (full path to file) does not exist, sqlite will create it. (However it will not
    create the directory - that is done in create_db_dir().)

    The sqlite3 library already has a context manager for the connection, which is what is
    yielded here. It will commit or rollback, depending on success or failures of commands
    given to it.

    However, it does not close the connection. So this context manager opens the connection,
    yields the context manager, and then closes the connection.
    """
    conn = sqlite3.connect(path)
    try:
        with conn:
            yield conn
    finally:
        conn.close()


def create_db() -> None:
    """Create database and tables if not already created."""
    with db(DB_PATH) as conn:
        try:
            conn.execute(
                """
                CREATE TABLE decks(
                    id INTEGER PRIMARY KEY, name TEXT, description TEXT, current_deck INTEGER
                )
                """
            )
        except sqlite3.OperationalError as e:  # exception raised if tables already exist
            if "already exists" in str(e):
                pass
            else:
                return click.echo(e)
        else:
            try:
                conn.execute(
                    """
                    CREATE TABLE cards(
                        id INTEGER PRIMARY KEY,
                        deck_id INTEGER,
                        question TEXT,
                        answer TEXT,
                        reversible INTEGER,
                        FOREIGN KEY(deck_id) REFERENCES decks(id)
                    )
                    """
                )
            except sqlite3.OperationalError as e:  # exception raised if tables already exist
                if "already exists" in str(e):
                    pass
                else:
                    return click.echo(e)


def create_deck(name: str, desc: str) -> None:
    """Create a deck and make it the selected deck."""
    with db(DB_PATH) as conn:
        conn.execute("INSERT INTO decks (name, description) VALUES (?, ?)", (name, desc))
    select_deck(name)


def select_deck(deck: str) -> None:
    """Set a deck as the selected deck."""
    with db(DB_PATH) as conn:
        if not conn.execute("SELECT * FROM decks where name = ?", (deck,)).fetchone():
            raise ValueError("No such deck.")
        conn.execute("UPDATE decks SET current_deck = '1' WHERE name = ?", (deck,))
        conn.execute("UPDATE decks SET current_deck = '0' WHERE name != ?", (deck,))


def get_deck(deck_name: str = "") -> Optional[Deck]:
    """Either get a specified deck or the selected deck."""
    with db(DB_PATH) as conn:
        if not deck_name:
            result = conn.execute("SELECT * FROM decks WHERE current_deck = '1'").fetchone()
        else:
            result = conn.execute("SELECT * FROM decks WHERE name = ?", (deck_name,)).fetchone()

        if not result:
            return None

        cards = []
        result_cards = conn.execute(
            "SELECT * FROM cards WHERE deck_id = ?", (result[0],)
        ).fetchall()

        if result_cards:
            for row in result_cards:
                cards.append(
                    {
                        "id": row[0],
                        "deck_id": row[1],
                        "question": row[2],
                        "answer": row[3],
                        "reversible": row[4],
                    }
                )

        deck = Deck(result[0], result[1], result[2], result[3], cards)

    return deck


def get_decks() -> list:
    """Get all decks."""
    decks: List[Optional[Deck]] = []
    with db(DB_PATH) as conn:
        result_deck = conn.execute("SELECT name from decks").fetchall()
        if not result_deck:
            return decks

    for row in result_deck:
        deck = get_deck(row[0])
        decks.append(deck)
    return decks


def add_card_to_deck(deck_id: int, card: dict) -> None:
    """Add card to a deck."""
    reversible = 0 if card["reversible"] == "no" else 1
    with db(DB_PATH) as conn:
        conn.execute(
            "INSERT INTO cards (deck_id, question, answer, reversible) VALUES (?, ?, ?, ?)",
            (deck_id, card["question"], card["answer"], reversible),
        )


def name_would_be_duplicate(name: str) -> bool:
    """
    Return True if deck name already exists, else False.

    Used to re-prompt user in check_deck_name.
    """
    with db(DB_PATH) as conn:
        result = conn.execute("SELECT id FROM decks where name = ?", (name,)).fetchone()

    if result:
        return True
    return False


def name_contains_prohibited_chars(name: str) -> bool:
    """
    Return True if prohibited characters included in deck name.

    Used to re-prompt user in check_deck_name.
    """
    allowed = list(string.ascii_letters) + list(string.digits) + ["-", "_"]
    for char in name:
        if char not in allowed:
            return True
    return False


def name_is_all(name: str) -> bool:
    """
    Return True if deck name is all, else False.

    Used to re-prompt user in check_deck_name.
    """
    if name == "all":
        return True
    return False


def check_deck_name(context, param, value) -> str:
    """Enforce contraints on the deck name."""

    while name_would_be_duplicate(value):
        click.echo("Sorry, a deck with that name already exists.")
        value = click.prompt("Name of the deck")

    while name_contains_prohibited_chars(value):
        click.echo(
            "Sorry, a deck name can only contain ASCII letters, numbers, underscores (_), and dashes (-)."
        )
        value = click.prompt("Name of the deck")

    while name_is_all(value):
        click.echo(
            "Sorry, a deck cannot be named 'all'. It's a keyword used in 'flashcards study all'."
        )
        value = click.prompt("Name of the deck")
    return value


def search_deck(deck_id: int, term: str) -> list:
    """Search a deck for term."""
    term = "%" + term + "%"
    with db(DB_PATH) as conn:
        result = conn.execute(
            """
            SELECT question, answer
            FROM cards
            WHERE (question LIKE ? OR answer LIKE ?) AND deck_id = ?
            """,
            (term, term, deck_id),
        ).fetchall()
    if result:
        search_results = []
        for row in result:
            search_results.append({"question": row[0], "answer": row[1]})
        return search_results
    return []
