"""Main entry point of the application, with commands and sub-commands."""
import random
import sqlite3

import click

from flashcards import decks
from flashcards.editor import edit_card, prompt_via_editor, remove_instructions


@click.group()
def cli():
    """
    Create and study flashcards on the command line.

    For additional help, run a command below with the --help option or visit https://codeberg.org/kdwarn/flashcards.
    """
    decks.create_db_dir()
    decks.create_db()


@cli.command("status")
def status_cmd():
    """Show details about selected deck, if any."""
    deck = decks.get_deck()
    if not deck:
        return click.echo("No deck currently selected.")

    click.echo(f"\nCurrently selected deck: {deck.name}")
    click.echo(f"Number of cards: {len(deck.cards)}")
    if deck.description:
        click.echo(f"Description: {deck.description}")
    click.echo("")


@cli.command("study")
@click.argument("deck_name", metavar="deck", default="")
@click.option(
    "-o",
    "--ordered",
    is_flag=True,
    help="Study the cards in the order they were added to the deck.",
)
def study_cmd(deck_name, ordered):
    """
    Start a study session. By default, the cards are shuffled.

    If DECK is not provided, study the selected deck, if any.

    Use "flashcards study all" to study cards from all decks.
    """
    cards_to_study = []

    if deck_name == "all":
        all_decks = decks.get_decks()
        if not all_decks:
            return click.echo("No decks have been created yet.")

        for deck in all_decks:
            cards_to_study += deck.cards

        if not cards_to_study:
            return click.echo("There are no cards to study.")

    else:
        deck = decks.get_deck(deck_name)
        if not deck:
            if not deck_name:
                return click.echo("No deck currently selected.")
            return click.echo("No deck by that name found.")

        if not deck.cards:
            return click.echo(f"The {deck.name} deck currently has no cards.")
        cards_to_study = deck.cards

    if not ordered:
        random.shuffle(cards_to_study)

    # study - iterate through cards, pausing for user input after each question/answer.
    question_num = len(cards_to_study)

    for i, card in enumerate(cards_to_study, start=1):
        click.echo(card)
        with decks.db(decks.DB_PATH) as conn:
            deck_name = conn.execute(
                "SELECT name FROM decks WHERE id = ?", (card["deck_id"],)
            ).fetchone()[0]

        if card["reversible"]:
            answer_and_question = [card["question"], card["answer"]]
            random.shuffle(answer_and_question)
            first = answer_and_question[0]
            second = answer_and_question[1]
        else:
            first = card["question"]
            second = card["answer"]

        click.clear()
        click.echo(f"QUESTION {i} / {question_num} ({deck_name} deck)")
        click.echo("\n" + first + "\n")
        click.pause("...")
        click.echo("\n" + second + "\n")
        click.secho(
            "Press 'e' to edit this question, 'd' to delete it, 'q' to quit, and any other key to show the next question.",
            fg="green",
        )
        key_press = click.getchar()  # note that this also acts as a pause

        if key_press == "q":
            return
        if key_press == "e":
            try:
                edited_card = edit_card(card)
            except ValueError as e:
                click.echo(e)
            else:
                with decks.db(decks.DB_PATH) as conn:
                    try:
                        conn.execute(
                            """
                                UPDATE cards
                                SET
                                    question = ?,
                                    answer = ?,
                                    reversible = ?
                                WHERE id = ?
                            """,
                            (
                                edited_card["question"],
                                edited_card["answer"],
                                edited_card["reversible"],
                                card["id"],
                            ),
                        )
                    except sqlite3.Error as e:
                        click.echo(f"Error saving edits: {e}")
                    else:
                        click.echo("Card edited.")
            click.pause()
        if key_press == "d":
            try:
                click.confirm(
                    "Are you sure you want to delete this card? It cannot be undeleted.", abort=True
                )
            except click.Abort:
                click.echo("Card not deleted.")
            else:
                with decks.db(decks.DB_PATH) as conn:
                    try:
                        conn.execute("DELETE FROM cards WHERE id = ?", (card["id"],))
                    except sqlite3.Error as e:
                        click.echo(f"Unable to delete card: {e}")
                    else:
                        click.echo("Card deleted.")

            click.pause()

    click.echo("All done!")


@cli.command("create")
@click.option("--name", prompt="Name of the deck", callback=decks.check_deck_name)
@click.option(
    "--desc",
    prompt="Description of the deck (leave empty for none)",
    default="",
    show_default=False,
)
def create(name, desc):
    """Create a new deck.

    Deck names can only contain ASCII letters, numbers, underscores (_), and dashes (-).
    """
    try:
        decks.create_deck(name, desc)
    except sqlite3.Error:
        click.echo("Unable to create deck.")
    else:
        click.echo(f'Deck "{name}" created! It is now the selected deck.')


@cli.command("select")
@click.argument("deck")
def select(deck):
    """Select a deck to add cards to and to study."""
    try:
        decks.select_deck(deck)
    except ValueError as e:
        click.echo(e)
    else:
        click.echo(f"{deck} deck selected.\nNew cards will be added to this deck.")


@cli.command("list")
def list_decks():
    """List all decks."""
    all_decks = decks.get_decks()
    if not all_decks:
        return click.echo("You don't have any decks yet.")

    click.echo("\nYour decks:")
    for deck in all_decks:
        click.echo("  ", nl=False)
        if deck.current_deck == 1:
            click.secho("* ", nl=False, bold=True)
            click.secho(f"{deck.name}", nl=False, underline=True, bold=True)
            click.echo(" ", nl=False)
        else:
            click.echo(f"{deck.name} ", nl=False)
        click.echo(f"({str(len(deck.cards))} cards)", nl=False)
        if deck.description:
            click.echo(f": {deck.description}")
        else:
            click.echo("")
    click.echo("* indicates selected deck, if any")
    click.echo("")


@cli.command("add")
@click.option(
    "-e",
    "editormode",
    is_flag=True,
    help="Use an editor rather than the command line to create the card.",
)
def add(editormode):
    """
    Add a card to the currently selected deck.

    Cards contain a question, an answer, and a setting ("reversible") to indicate if the
    question and answer can be reversed when studying.
    """
    deck = decks.get_deck()
    if not deck:
        return click.echo("No deck is currently selected. Select a deck to add a card.")

    if editormode:
        try:
            question = prompt_via_editor("\n# Write your question above.")
            question = remove_instructions(question).strip()
            if not question:
                return click.echo("Card not added - no question entered.")
            answer = prompt_via_editor("\n# Write your answer above.")
            answer = remove_instructions(answer).strip()
            if not answer:
                return click.echo("Card not added - no answer entered.")
            reversible = click.prompt(
                "Should the card be reversible?", type=click.Choice(["yes", "no"])
            )
            card = {"question": question, "answer": answer, "reversible": reversible}
        except FileNotFoundError:
            return click.echo(
                "Could not open an editor. Set the EDITOR environment variable to the name "
                "of your editor or install Vim."
            )
    else:
        card = {}
        card["question"] = click.prompt("Question")
        card["answer"] = click.prompt("Answer")
        card["reversible"] = click.prompt("Reversible?", type=click.Choice(["yes", "no"]))

    decks.add_card_to_deck(deck.id, card)
    click.echo("Card added to the deck!")


@cli.command("search")
@click.argument("term")
def search(term):
    """Search selected deck for term."""
    all_decks = decks.get_decks()
    if not all_decks:
        return click.echo("You don't have any decks yet.")

    selected_deck = decks.get_deck()
    if not selected_deck:
        return click.echo("No deck currently selected.")

    results = decks.search_deck(selected_deck.id, term)

    if not results:
        return click.echo(f"Search term not found in {selected_deck.name}")

    click.echo("")
    click.echo("Results:")
    for result in results:
        click.echo(f"Question: {result['question']}")
        click.echo(f"Answer: {result['answer']}")
        click.echo("")
