[![codecov](https://codecov.io/gh/kdwarn/flashcards/branch/main/graphs/badge.svg)](https://codecov.io/gh/kdwarn/flashcards)

# flashcards

Application for creating and studying flashcards on the command line. Forked from [zergov/flashcards](https://github.com/zergov/flashcards).

![](demo.gif)

## Installation

Use [pipx](https://github.com/pipxproject/pipx):

```
$ pipx install git+https://codeberg.org/kdwarn/flashcards.git
```

## Creating a Deck and Adding Cards

The app stores flashcards in decks. To create a deck, run `flashcards create`. You will then be prompted to provide a name and a description of the deck.

To then add a card to the deck, run `flashcards add` and you will be prompted to provide a question and answer for the card and indicate if it's reversible or not.

You can also write the question and answer inside an editor by passing the `-e` parameter to the `flashcards add` command: `flashcards add -e`. This will open vim by default or the editor set to your `EDITOR` environment variable.

Add as many cards as you like.

## The Selected Deck and Deck Status

By default, after creating a deck, the application automatically selects it. New cards you create will be added to this deck (until you select a different one). Also, the `flashcards status` command will show information on the currently selected deck:

```
$ flashcards status

Currently selected deck: Math
Number of cards: 1
Description: Some math questions
```

To select a different deck:

```
$ flashcards select French

Selected deck: French
New cards will be added to this deck.
```

## Studying Decks, Editing and Deleting Cards

You can study the currently selected deck with `flashcards study` or you can specify a different deck, e.g. `flashcards study German`. If you have more than one deck, you can also study them all at once with `flashcards study all`.

The app will iterate through the cards, pausing between the question and answer. (And if a card has been marked as being reversible, the question or answer will randomly appear first.) After both the question and answer have been displayed, you can edit the card by pressing "e", delete the card by pressing "d", and quit the session by pressing "q".

By default, the cards will be shuffled before you start studying. To display them in the order they were created in, pass the `--ordered` flag: `flashcards study German --ordered`.

## Searching Decks

Forget if you've already added something to a deck? Search for it with `flashcards search "word_here"`. The quotes are optional when searching for a single word, but they are necessary if you search for a phrase: `flashcards search "how to quit vim"`.

## Upgrading from 1.x to 2.x

The storage format was changed in version 2.0 from individual json files for each deck to one sqlite database. If you are upgrading from v1.x to v2.x, you must first upgrade to v2.0 (`pipx install https://codeberg.org/kdwarn/flashcards/archive/v2.0.tar.gz`). That version will allow you to convert from the old format to the new. After installing version 2.0, simply run `flashcards import`. A new `sqlite.db` file will be created in the `~/.flashcards` directory, along with your old files. If you'd like to delete the old files, please first verify that the cards have been imported properly. (And if not, create an issue here.) Then you can continue on to the latest version (see installation instructions above).
