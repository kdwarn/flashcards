import os
from unittest.mock import patch

from click.testing import CliRunner
import pytest

from flashcards import main
from flashcards import decks

################
# Status command


def test_success_status(math_deck):
    runner = CliRunner()
    result = runner.invoke(main.status_cmd)
    assert "Math" in result.output
    assert "For learning basic arithmetic" in result.output
    assert "Number of cards: 4" in result.output


def test_success_no_deck(mock_db):
    runner = CliRunner()
    result = runner.invoke(main.status_cmd)
    assert "No deck currently selected" in result.output


###############
# Study command

"""
NOTE: in main.study_cmd(), the program is paused twice on each card - first between the question and
the answer by click.pause(), and then after the answer by click.getchar(). However, if click.pause
is not run from a terminal, it's as if nothing happens, so this does not happen during testing.
So it's as if there's only one pause for user input to be entered. Unlike click.prompt, when
entering input for this, you only need to string together as many single key-strokes as desired (and
two per card), rather than separating them with \n. If there are not enough inputs to for every
question, the program will just continue until the end.
"""


def test_success_study(math_deck):
    runner = CliRunner()
    result = runner.invoke(main.study_cmd, ["Math"])
    assert "All done!" in result.output


def test_error_message_if_requested_deck_doesnt_exist(mock_db):
    runner = CliRunner()
    result = runner.invoke(main.study_cmd, ["American"])
    assert "No deck by that name" in result.output


def test_error_message_if_no_deck_specified_and_no_selected_deck(mock_db):
    runner = CliRunner()
    result = runner.invoke(main.study_cmd)
    assert "No deck currently selected" in result.output


def test_use_selected_deck_if_no_deck_specified(math_deck):
    runner = CliRunner()
    result = runner.invoke(main.select, ["Math"])
    result = runner.invoke(main.study_cmd)
    assert "2 + 2" in result.output
    assert "All done!" in result.output


def test_error_message_if_deck_has_no_cards(german_deck):
    runner = CliRunner()
    result = runner.invoke(main.study_cmd, ["German"])
    assert "has no cards" in result.output


def test_error_message_if_multiple_decks_have_no_cards(german_deck, italian_deck):
    runner = CliRunner()
    result = runner.invoke(main.study_cmd, ["all"])
    assert "no cards to study" in result.output


def test_error_message_if_study_all_but_no_decks(mock_db):
    decks.create_db()
    runner = CliRunner()
    result = runner.invoke(main.study_cmd, ["all"])
    assert "No decks have been created yet." in result.output


def test_pressing_q_quits_session(math_deck):
    runner = CliRunner()
    result = runner.invoke(main.study_cmd, ["Math", "-o"], input="jq")
    assert "2 + 2 = ?" in result.output  # first question should be in output
    assert "2 + 3 = ?" in result.output  # second question should be in output
    assert "2 + 4 = ?" not in result.output  # should not reach second question
    assert "All done!" not in result.output  # should not reach the end


def test_pressing_e_enters_editor(math_deck):
    """User edits first question and then quits after second question."""
    runner = CliRunner()
    result = runner.invoke(main.study_cmd, ["Math", "-o"], input="eq")

    # 1st question should be output - appears before the first input is received/checked
    assert "2 + 2 = ?" in result.output

    # No edits occurred, so that message should then be displayed after entering/exiting editor
    assert "No edits detected" in result.output

    # 2nd question should be output - user just hit e, so it continues (after printing debug)
    assert "2 + 3 = ?" in result.output

    # nothing after the 2nd question - user hit "q" after 2nd question
    assert "2 + 4 = ?" not in result.output
    assert "All done!" not in result.output


@pytest.mark.xfail
def test_edited_card_is_saved(math_deck):
    assert False


def test_delete_card_success(math_deck):
    """Delete first card in math deck."""
    assert len(math_deck.cards) == 4
    runner = CliRunner()
    runner.invoke(main.study_cmd, ["Math", "-o"], input="dy\n")

    # need to reload the deck to test if card was removed
    math_deck = decks.get_deck("Math")
    assert len(math_deck.cards) == 3


def test_cancel_delete_does_not_delete(math_deck):
    assert len(math_deck.cards) == 4
    runner = CliRunner()
    runner.invoke(main.study_cmd, ["Math", "-o"], input="dN\n")

    # need to reload the deck to test if card was removed
    math_deck = decks.get_deck("Math")
    assert len(math_deck.cards) == 4


################
# create command


def test_create_success_message_received(mock_db):
    decks.create_db()
    runner = CliRunner()
    result = runner.invoke(main.create, input="Italian\nStudy Italian")
    assert 'Deck "Italian" created' in result.output


def test_deck_created(mock_db):
    decks.create_db()
    runner = CliRunner()
    runner.invoke(main.create, input="Italian\nStudy Italian")
    deck = decks.get_deck("Italian")
    assert deck.name == "Italian"


def test_reprompt_if_deck_name_already_exists(math_deck):
    runner = CliRunner()

    # "Math" already exists so user is reprompted; then we submit acceptable name and desc
    result = runner.invoke(main.create, input="Math\nMath1\nLearning math.")

    assert "Sorry, a deck with that name already exists." in result.output
    assert 'Deck "Math1" created' in result.output

    # now check the name is correct
    deck = decks.get_deck("Math1")
    assert deck.name == "Math1"


def test_reprompt_if_deck_name_is_all(mock_db):
    decks.create_db()
    runner = CliRunner()
    result = runner.invoke(main.create, input="all\ntester\nJust testing.")

    assert "Sorry, a deck cannot be named 'all'" in result.output


def test_deck_made_current_deck_after_creation(mock_db):
    runner = CliRunner()
    runner.invoke(main.create, input="Italian\nStudy Italian")
    deck = decks.get_deck("Italian")
    assert deck.current_deck == 1


################
# select command


def test_successful_deck_selection(math_deck, french_deck):
    runner = CliRunner()
    result = runner.invoke(main.select, ["Math"])
    assert "Math deck selected." in result.output


def test_selecting_deck_that_doesnt_exist_returns_error_message(mock_db):
    runner = CliRunner()
    result = runner.invoke(main.select, ["spanish"])
    assert "No such deck." in result.output


def test_deck_made_current_deck_after_being_selected(math_deck, french_deck):
    runner = CliRunner()
    runner.invoke(main.select, ["Math"])
    deck = decks.get_deck("Math")
    assert deck.current_deck == 1


#############
# add command


def test_add_card_successful(math_deck):
    runner = CliRunner()
    result = runner.invoke(main.add, input="Square root of 25?\n5\nyes")
    assert "Card added to the deck!" in result.output


def test_add_card_returns_error_message_if_no_deck_selected(mock_db):
    runner = CliRunner()
    result = runner.invoke(main.add, input="Square root of 25?\n5\nno")
    assert "No deck is currently selected" in result.output


@patch.dict(os.environ, {"EDITOR": "not_an_editor"})
def test_check_if_no_editor_env_var_and_no_vim_returns_error_message(math_deck):
    runner = CliRunner()
    result = runner.invoke(main.add, ["-e"])
    assert "Could not open" in result.output


######################
# list decks command


def test_listing_decks_successful(math_deck):
    runner = CliRunner()
    result = runner.invoke(main.list_decks)
    assert "Your decks:" in result.output and "Math" in result.output


def test_list_multiple_decks_successful(math_deck, german_deck):
    runner = CliRunner()
    result = runner.invoke(main.list_decks)
    assert "Your decks:" in result.output and "Math" in result.output and "German" in result.output


def test_error_message_if_no_decks_after_list_command(mock_db):
    runner = CliRunner()
    result = runner.invoke(main.list_decks)
    assert "You don't have any decks yet." in result.output


@pytest.mark.xfail
def test_decks_listed_in_alpha_order(german_deck, math_deck):
    assert False


#####################
# search command


def test_search_deck_informs_no_results(math_deck):
    runner = CliRunner()
    result = runner.invoke(main.search, ["this phrase not in math deck"])
    assert "Search term not found in " in result.output


def test_search_deck_returns_result(math_deck):
    runner = CliRunner()
    result = runner.invoke(main.search, ["2 + 2"])
    assert "Question" in result.output
    assert "Answer" in result.output
    assert "4" in result.output


def test_search_returns_error_message_if_no_deck(mock_db):
    runner = CliRunner()
    result = runner.invoke(main.search, ["help"])
    assert "You don't have any decks yet." in result.output
