import os
from pathlib import Path

import pytest

from flashcards import decks


@pytest.fixture
def mock_db(tmp_path, monkeypatch):
    """Monkeypatch decks.DB_PATH() so that we use a tmp path."""
    monkeypatch.setattr(decks, "DB_PATH", Path(tmp_path / decks.DB_FILE))
    decks.create_db()


@pytest.fixture
def french_deck(mock_db):
    decks.create_deck("French", "A french deck")
    deck = decks.get_deck("French")
    card1 = {"question": "bonjour", "answer": "hello", "reversible": "yes"}
    decks.add_card_to_deck(deck.id, card1)
    deck = decks.get_deck("French")

    return deck


@pytest.fixture
def math_deck(mock_db):
    decks.create_deck("Math", "For learning basic arithmetic.")
    deck = decks.get_deck("Math")
    cards = [
        {"question": "2 + 2 = ?", "answer": "4", "reversible": "no"},
        {"question": "2 + 3 = ?", "answer": "5", "reversible": "no"},
        {"question": "2 + 4 = ?", "answer": "6", "reversible": "no"},
        {"question": "2 + 5 = ?", "answer": "7", "reversible": "no"},
    ]
    for card in cards:
        decks.add_card_to_deck(deck.id, card)
    deck = decks.get_deck("Math")

    return deck


@pytest.fixture
def german_deck(mock_db):
    """A deck with no cards."""
    decks.create_deck("German", "Learning German.")
    deck = decks.get_deck("German")

    return deck


@pytest.fixture
def italian_deck(mock_db):
    """Another deck with no cards."""
    decks.create_deck("Italian", "Learning Italian")
    deck = decks.get_deck("Italian")
    return deck
