from pathlib import Path

import pytest

from flashcards import decks


# DB_DIR_NAME = ".flashcards"
# DB_FILE = "sqlite.db"


def test_mock_db(mock_db):
    """Fixture should create db in tmp directory."""
    decks.create_db()
    assert decks.DB_PATH.exists()
    assert str(decks.DB_PATH.resolve()).endswith("sqlite.db")


def test_db_dir():
    assert decks.db_dir() == Path.home() / decks.DB_DIR_NAME


def test_get_name(math_deck):
    assert math_deck.name == "Math"


def test_get_description(math_deck):
    assert math_deck.description == "For learning basic arithmetic."


def test_len_deck1(math_deck):
    assert len(math_deck.cards) == 4


def test_len_deck2(french_deck):
    print(french_deck.name, french_deck.description, french_deck.cards)
    assert len(french_deck.cards) == 1


def test_add_card_success(math_deck):
    decks.add_card_to_deck(math_deck.id, {"question": "5x5", "answer": "25", "reversible": "no"})
    math_deck = decks.get_deck("Math")
    assert len(math_deck.cards) == 5


@pytest.mark.parametrize("name, expected", [("Math", True), ("German", True), ("English", False)])
def test_name_would_be_duplicate(name, expected, math_deck, german_deck):
    assert decks.name_would_be_duplicate(name) is expected


def test_deck_cannot_be_named_all():
    assert decks.name_is_all("all") is True


def test_loading_non_existant_deck_returns_none(mock_db):
    deck = decks.get_deck("Finnish")
    assert not deck


################
# search command


def test_search_returns_list(math_deck):
    result = decks.search_deck(math_deck.id, "anything should return a list")
    assert isinstance(result, list)


def test_search_miss_returns_empty_list(math_deck):
    result = decks.search_deck(math_deck.id, "not a term in the math deck")
    assert isinstance(result, list)
    assert len(result) == 0


def test_search_hit_returns_list_of_dicts(math_deck):
    result = decks.search_deck(math_deck.id, "2 + 2")
    assert len(result) == 1
    assert isinstance(result[0], dict)


def test_search_hit_one_result_correct_data(math_deck):
    result = decks.search_deck(math_deck.id, "2 + 2")
    assert result[0]["question"] == "2 + 2 = ?"
    assert result[0]["answer"] == "4"


def test_search_hit_multi_results_correct_data(math_deck):
    result = decks.search_deck(math_deck.id, "2 +")
    assert len(result) == 4
    assert result[0]["question"] == "2 + 2 = ?"
    assert result[0]["answer"] == "4"
    assert result[1]["question"] == "2 + 3 = ?"
    assert result[1]["answer"] == "5"
    assert result[2]["question"] == "2 + 4 = ?"
    assert result[2]["answer"] == "6"
    assert result[3]["question"] == "2 + 5 = ?"
    assert result[3]["answer"] == "7"
